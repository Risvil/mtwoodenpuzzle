Welcome to Multi-Touch Wooden Puzzle!
=====================

### Disclaimer: This is a work in progress. ###

**Multi-Touch Wooden Puzzle** is a sample application implemented using Keep-In Touch (https://bitbucket.org/Risvil/keepinmove). Simulates a wooden piece puzzle like the ones little childs plays with. Each piece can be manipulated (moved, rotated, scaled) simultaneously and independently.

----------