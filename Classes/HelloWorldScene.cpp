#include "HelloWorldScene.h"
#include "KimSprite.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	// Add kim close button, size to match closeItem from menu
	auto closeButton = KimSprite::create();
	closeButton->setContentSize(closeItem->getContentSize());
	closeButton->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
		origin.y + closeItem->getContentSize().height / 2));

	closeButton->setOnTouchUpDelegate([](const kim::TouchEvent& e) {
		Director::getInstance()->end();
	});
	addChild(closeButton, 2);


	// add a "new file" icon to clear the background
	auto newFileItem = MenuItemImage::create(
		"NewFileNormal.png",
		"NewFileSelected.png",
		CC_CALLBACK_1(HelloWorld::menuNewFileCallback, this));

	newFileItem->setPosition(Vec2(origin.x + visibleSize.width - newFileItem->getContentSize().width * 2,
		origin.y + newFileItem->getContentSize().height / 2));

	// Add kim new button, size to match closeItem from menu
	auto newFileButton = KimSprite::create();
	newFileButton->setContentSize(newFileItem->getContentSize());
	newFileButton->setPosition(Vec2(origin.x + visibleSize.width - newFileItem->getContentSize().width / 2,
		origin.y + newFileItem->getContentSize().height / 2));

	newFileButton->setOnTouchUpDelegate([this](const kim::TouchEvent& e) {
		this->onNewFile();
	});
	addChild(newFileButton, 2);

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->addChild(newFileItem);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 2);

    /////////////////////////////
    // 3. add your codes below...

	// add keyboard and mouse support
	auto mouseListener = EventListenerMouse::create();
	mouseListener->onMouseDown = CC_CALLBACK_1(HelloWorld::onMouseDown, this);
	mouseListener->onMouseMove = CC_CALLBACK_1(HelloWorld::onMouseMove, this);
	mouseListener->onMouseUp = CC_CALLBACK_1(HelloWorld::onMouseUp, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);

	auto keyListener = EventListenerKeyboard::create();
	keyListener->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyUp, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyListener, this);

	m_mode = EManipulationMode::Move;
	bMousePressed = false;

	// add background
	auto spriteBackground = Sprite::create("BackgroundTex.jpg");
	spriteBackground->setContentSize(visibleSize);
	spriteBackground->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	addChild(spriteBackground, 0);

	// init KIM
	m_pEngine = kim::IEngine::CreateEngine();
	if (m_pEngine->Init())
	{
		// Register touchable regions
		m_pEngine->RegisterRegion(newFileButton, kim::IEngine::Priority::FOREGROUND);
		m_pEngine->RegisterRegion(closeButton, kim::IEngine::Priority::FOREGROUND);
		//m_pEngine->RegisterRegion(m_spriteBackground, kim::IEngine::Priority::BACKGROUND);
		m_pEngine->Run();
	}

	m_clickPosition = Vec2();
	m_currentPiece = nullptr;

	m_pPuzzle = new PuzzleTable(this);
	if (m_pPuzzle != nullptr)
	{
		m_pPuzzle->initPuzzle();
	}
    
    return true;
}

void HelloWorld::addTouchableSprite(Sprite* sprite, EScreenLocation location)
{
	addChild(sprite, 1);

	KimSprite* kimSprite = static_cast<KimSprite*>(sprite);
	if (kimSprite != nullptr && m_pEngine != nullptr)
	{
		kim::IEngine::Priority priority;
		switch (location)
		{
		case EScreenLocation::Background:
			priority = kim::IEngine::Priority::BACKGROUND;
			break;

		case EScreenLocation::Foreground:
			priority = kim::IEngine::Priority::FOREGROUND;
			break;

		default:
			priority = kim::IEngine::Priority::FOREGROUND;
			break;
		}

		m_pEngine->RegisterRegion(kimSprite, priority);
	}
}

void HelloWorld::removeTouchableSprite(Sprite* sprite)
{
	removeChild(sprite);

	KimSprite* kimSprite = static_cast<KimSprite*>(sprite);
	if (kimSprite != nullptr && m_pEngine != nullptr)
	{
		m_pEngine->UnregisterRegion(kimSprite);
	}
}

void HelloWorld::menuNewFileCallback(cocos2d::Ref* pSender)
{
	onNewFile();
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::onNewFile()
{
	if (m_pPuzzle != nullptr)
	{
		m_pPuzzle->initPuzzle();
	}
}

void HelloWorld::onExit()
{
	Layer::onExit();

	if (m_pEngine != nullptr)
	{
		m_pEngine->Shutdown();
	}
}

void HelloWorld::onMouseDown(cocos2d::Event* event)
{
	bMousePressed = true;

	auto e = dynamic_cast<EventMouse*>(event);
	if (e != nullptr && m_pPuzzle != nullptr)
	{
		m_clickPosition.x = e->getCursorX();
		m_clickPosition.y = e->getCursorY();

		for (auto piece : m_pPuzzle->getPieces())
		{
			if (piece != nullptr && piece->getBoundingBox().containsPoint(m_clickPosition))
			{
				m_currentPiece = piece;
			}
		}
	}
}

void HelloWorld::onMouseMove(cocos2d::Event* event)
{
	if (bMousePressed && m_currentPiece != nullptr && !m_currentPiece->isFitted())
	{
		auto e = dynamic_cast<EventMouse*>(event);
		if (e != nullptr)
		{
			Vec2 currentClick;
			currentClick.x = e->getCursorX();
			currentClick.y = e->getCursorY();

			auto amount = m_clickPosition.y - currentClick.y;

			Vec2 position;
			float rotation;
			float scale;
			switch (m_mode)
			{
			case EManipulationMode::Move:
				position = m_currentPiece->getPosition();
				position += currentClick - m_clickPosition;
				m_currentPiece->setPosition(position);
				break;

			case EManipulationMode::Rotate:
				rotation = m_currentPiece->getRotation();
				rotation += amount;
				m_currentPiece->setRotation(rotation);
				break;

			case EManipulationMode::Scale:
				scale = m_currentPiece->getScale();
				scale += (amount * 0.01f);
				m_currentPiece->setScale(scale);
				break;
			}

			m_clickPosition = currentClick;
		}
	}
}

void HelloWorld::onMouseUp(cocos2d::Event* event)
{
	if (m_currentPiece != nullptr && !m_currentPiece->isFitted())
	{
		m_currentPiece->testOverlap();
	}

	bMousePressed = false;
	m_currentPiece = nullptr;
}

void HelloWorld::onKeyUp(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	switch (keyCode)
	{
	case cocos2d::EventKeyboard::KeyCode::KEY_Q:
		m_mode = EManipulationMode::Move;
		break;

	case cocos2d::EventKeyboard::KeyCode::KEY_W:
		m_mode = EManipulationMode::Rotate;
		break;

	case cocos2d::EventKeyboard::KeyCode::KEY_E:
		m_mode = EManipulationMode::Scale;
		break;

	default:
		m_mode = EManipulationMode::Move;
		break;
	}
}
