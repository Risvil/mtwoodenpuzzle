#include "PuzzlePiece.h"
#include "PuzzleTable.h"
#include "PuzzleHole.h"


PuzzlePiece::PuzzlePiece()
	: m_world(nullptr)	
	, m_dragGesture(nullptr)
	, m_zoomGesture(nullptr)
	, m_rotateGesture(nullptr)
	, bIsFitted(false)
	, m_shape(EShape::None)
{
}

PuzzlePiece* PuzzlePiece::create()
{
	PuzzlePiece* pSprite = new PuzzlePiece();

	if (pSprite->init())
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();

		return pSprite;
	}

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

PuzzlePiece* PuzzlePiece::create(const char* fileName)
{
	PuzzlePiece* pSprite = new PuzzlePiece();

	if (pSprite->initWithFile(fileName))
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();
		pSprite->AddEvents();

		return pSprite;
	}

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

bool PuzzlePiece::initGestures()
{
	m_dragGesture = std::make_shared<kim::DragGesture>();
	m_zoomGesture = std::make_shared<kim::ZoomPinchGesture>();
	m_rotateGesture = std::make_shared<kim::RotateGesture>();

	return true;
}

void PuzzlePiece::initPiece(PuzzleTable* world)
{
	m_world = world;

	bIsFitted = false;

	AddAllowedGesture(m_dragGesture);
	AddAllowedGesture(m_zoomGesture);
	AddAllowedGesture(m_rotateGesture);
}

bool PuzzlePiece::isFitted() const
{
	return bIsFitted;
}

void PuzzlePiece::onTouchUp(const kim::TouchEvent& e)
{
	testOverlap();
}

void PuzzlePiece::setShape(EShape shape)
{
	m_shape = shape;
}

EShape PuzzlePiece::getShape() const
{
	return m_shape;
}

void PuzzlePiece::fit(PuzzleHole* hole)
{
	if (hole != nullptr)
	{
		setPosition(hole->getPosition());
		setRotation(hole->getRotation());
		setScale(hole->getScale());
	}

	bIsFitted = true;

	RemoveAllowedGesture(m_dragGesture);
	RemoveAllowedGesture(m_zoomGesture);
	RemoveAllowedGesture(m_rotateGesture);

	if (m_world != nullptr)
	{
		m_world->addScore();
	}
}

void PuzzlePiece::testOverlap()
{
	if (m_world != nullptr)
	{
		for (auto& hole : m_world->getHoles())
		{
			if (hole != nullptr && hole->canFit(this))
			{
				// mark as fitted and deny manipulation for this piece
				fit(hole);
			}
		}
	}
}