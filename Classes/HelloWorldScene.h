#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "kim.h"
#include "PuzzleTable.h"
#include "PuzzlePiece.h"
#include <memory>

enum class EScreenLocation
{
	Background,
	Foreground,
};

enum class EManipulationMode
{
	Move,
	Rotate,
	Scale,
};

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

	void addTouchableSprite(cocos2d::Sprite* sprite, EScreenLocation location);
	void removeTouchableSprite(cocos2d::Sprite* sprite);
    
    // a selector callback
	void menuNewFileCallback(cocos2d::Ref* pSender);
    void menuCloseCallback(cocos2d::Ref* pSender);

	void onNewFile();
	virtual void onExit() override;

	void onMouseDown(cocos2d::Event* event);
	void onMouseMove(cocos2d::Event* event);
	void onMouseUp(cocos2d::Event* event);

	void onKeyUp(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

private:
	std::shared_ptr<kim::IEngine> m_pEngine;

	PuzzleTable* m_pPuzzle;

	EManipulationMode m_mode;
	bool bMousePressed;
	cocos2d::Vec2 m_clickPosition;
	PuzzlePiece* m_currentPiece;
};

#endif // __HELLOWORLD_SCENE_H__
