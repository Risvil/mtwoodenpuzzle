#ifndef PUZZLETABLE_H
#define PUZZLETABLE_H

#include <vector>

class HelloWorld;
class PuzzlePiece;
class PuzzleHole;
class PuzzleTable
{
public:
	PuzzleTable(HelloWorld* scene);
	~PuzzleTable();

	void initPuzzle();
	void addScore();
	bool hasGameEnded();

	std::vector<PuzzlePiece*>& getPieces();
	std::vector<PuzzleHole*>& getHoles();

private:
	std::vector<PuzzlePiece*> m_pieces;
	std::vector<PuzzleHole*> m_holes;

	HelloWorld* m_scene;
};

#endif // PUZZLETABLE_H