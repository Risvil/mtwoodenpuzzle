#ifndef PUZZLEPIECE_H
#define PUZZLEPIECE_H

#include "kim.h"
#include "KimSprite.h"
#include "GlobalTypes.h"
#include <memory>

class PuzzleHole;
class PuzzleTable;
class PuzzlePiece : public KimSprite
{
public:
	PuzzlePiece();

	static PuzzlePiece* create();
	static PuzzlePiece* create(const char* fileName);

	bool initGestures();

	void initPiece(PuzzleTable* world);
	bool isFitted() const;
	void onTouchUp(const kim::TouchEvent& e);

	void setShape(EShape shape);
	EShape getShape() const;

	void testOverlap();

private:
	void fit(PuzzleHole* hole);

private:
	PuzzleTable* m_world;

	std::shared_ptr<kim::DragGesture> m_dragGesture;
	std::shared_ptr<kim::ZoomPinchGesture> m_zoomGesture;
	std::shared_ptr<kim::RotateGesture> m_rotateGesture;

	bool bIsFitted;

	EShape m_shape;
};

#endif // PUZZLEPIECE_H