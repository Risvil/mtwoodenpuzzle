#ifndef GLOBALTYPES_H
#define GLOBALTYPES_H

enum class EShape
{
	None,
	Square,
	Triangle,
	Circle,
	Hexagon,
};

#endif // PUZZLETABLE_H