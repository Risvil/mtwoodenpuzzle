#include "PuzzleTable.h"
#include "PuzzleHole.h"
#include "PuzzlePiece.h"
#include "HelloWorldScene.h"
#include "GlobalTypes.h"

#include "cocos2d.h"

PuzzleTable::PuzzleTable(HelloWorld* scene)
	: m_scene(scene)
{
	// create pieces
	auto circlePiece = PuzzlePiece::create("CirclePieceTex.png");
	circlePiece->setShape(EShape::Circle);

	auto squarePiece = PuzzlePiece::create("SquarePieceTex.png");
	squarePiece->setShape(EShape::Square);

	auto trianglePiece = PuzzlePiece::create("TrianglePieceTex.png");
	trianglePiece->setShape(EShape::Triangle);

	auto hexPiece = PuzzlePiece::create("HexaPieceTex.png");
	hexPiece->setShape(EShape::Hexagon);

	m_pieces.push_back(circlePiece);
	m_pieces.push_back(squarePiece);
	m_pieces.push_back(trianglePiece);
	m_pieces.push_back(hexPiece);

	for (auto& piece : m_pieces)
	{
		if (piece != nullptr)
		{
			piece->retain();
			piece->initGestures();
			if (m_scene != nullptr)
			{
				m_scene->addTouchableSprite(piece, EScreenLocation::Background);
			}
		}
	}

	// create holes
	auto circleHole = PuzzleHole::create("CircleHoleTex.jpg");
	circleHole->setShape(EShape::Circle);

	auto squareHole = PuzzleHole::create("SquareHoleTex.jpg");
	squareHole->setShape(EShape::Square);

	auto triangleHole = PuzzleHole::create("TriangleHoleTex.jpg");
	triangleHole->setShape(EShape::Triangle);

	auto hexHole = PuzzleHole::create("HexaHoleTex.jpg");
	hexHole->setShape(EShape::Hexagon);

	m_holes.push_back(circleHole);
	m_holes.push_back(squareHole);
	m_holes.push_back(triangleHole);
	m_holes.push_back(hexHole);

	for (auto& hole : m_holes)
	{
		if (hole != nullptr)
		{
			hole->retain();
			if (m_scene != nullptr)
			{
				m_scene->addChild(hole, 0);
			}
		}
	}
}

PuzzleTable::~PuzzleTable()
{	
	for (auto& piece : m_pieces)
	{
		if (piece != nullptr)
		{
			piece->release();
		}
	}

	for (auto& hole : m_holes)
	{
		if (hole != nullptr)
		{
			hole->release();
		}
	}
}

void PuzzleTable::initPuzzle()
{
	auto const PuzzleScale = 0.25f;

	cocos2d::Size sceneSize(0, 0);
	if (m_scene != nullptr)
	{
		sceneSize = m_scene->getContentSize();
	}

	// init holes
	for (size_t i = 0; i < m_holes.size(); ++i)
	{
		auto hole = m_holes[i];
		if (hole != nullptr)
		{
			// Set position
			hole->setScale(PuzzleScale);

			auto holeWidth = hole->getBoundingBox().size.width;
			auto holeHeight = hole->getBoundingBox().size.height;

			cocos2d::Vec2 position;
			position.x = holeWidth + holeWidth / 4 + holeWidth * i + holeWidth / 2 * i;
			position.y = sceneSize.height / 2;

			hole->setPosition(position);
		}
	}

	// init pieces
	for (auto& piece : m_pieces)
	{
		if (piece != nullptr)
		{
			// Random position, scale and rotation.
			piece->initPiece(this);

			// Set rotation
			auto rotation = CCRANDOM_0_1() * 360.f;
			piece->setRotation(rotation);

			// Set scale
			const auto MinScale = 0.75f;
			const auto MaxScale = 2.f;
			auto scale = std::max(MinScale, CCRANDOM_0_1() * MaxScale);
			scale *= PuzzleScale;
			piece->setScale(scale);

			// Set position
			auto widthOffset = piece->getBoundingBox().size.width / 2;
			auto heightOffset = piece->getBoundingBox().size.height / 2;

			auto x = CCRANDOM_0_1() * sceneSize.width + widthOffset;
			auto y = CCRANDOM_0_1() * sceneSize.height + heightOffset;

			if (x + widthOffset > sceneSize.width)
			{
				x = sceneSize.width - piece->getBoundingBox().size.width;
			}
			if (y + heightOffset > sceneSize.height)
			{
				y = sceneSize.height - piece->getBoundingBox().size.height;
			}

			piece->setPosition(cocos2d::Vec2(x, y));

			// TODO:  Avoid overlapping with holes and other pieces?
		}
	}
}

void PuzzleTable::addScore()
{
	if (hasGameEnded())
	{
		// Show message
	}
}

bool PuzzleTable::hasGameEnded()
{
	for (auto& piece : getPieces())
	{
		if (piece != nullptr && !piece->isFitted())
		{
			return false;
		}
	}

	return true;
}

std::vector<PuzzlePiece*>& PuzzleTable::getPieces()
{
	return m_pieces;
}

std::vector<PuzzleHole*>& PuzzleTable::getHoles()
{
	return m_holes;
}