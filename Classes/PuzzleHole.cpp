#include "PuzzleHole.h"
#include "PuzzlePiece.h"

PuzzleHole::PuzzleHole()
	: m_shape(EShape::None)
{

}

PuzzleHole* PuzzleHole::create()
{
	PuzzleHole* pSprite = new PuzzleHole();

	if (pSprite->init())
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();

		return pSprite;
	}

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

PuzzleHole* PuzzleHole::create(const char* fileName)
{
	PuzzleHole* pSprite = new PuzzleHole();

	if (pSprite->initWithFile(fileName))
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();
		pSprite->AddEvents();

		return pSprite;
	}

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

bool PuzzleHole::canFit(PuzzlePiece* piece) const
{
	if (piece == nullptr)
	{
		return false;
	}

	EShape pieceShape = piece->getShape();
	if (pieceShape != getShape())
	{
		return false;
	}

	// Check distance
	static const auto AllowedDistanceRatio = 0.1f;

	auto piecePosition = piece->getPosition();
	auto position = getPosition();

	auto positionRatioX = piecePosition.x / position.x;
	auto positionRatioY = piecePosition.y / position.y;

	if (positionRatioX > 1 + AllowedDistanceRatio || positionRatioX < 1 - AllowedDistanceRatio || positionRatioY > 1 + AllowedDistanceRatio || positionRatioY < 1 - AllowedDistanceRatio)
	{
		return false;
	}

	// Check scale
	static const auto AllowedScaleRatio = 0.1f;

	auto scaleRatio = std::abs(piece->getScale() / getScale());
	if (scaleRatio > 1 + AllowedScaleRatio || scaleRatio < 1 - AllowedScaleRatio)
	{
		return false;
	}

	// Check rotation
	static const auto AllowedRotationRatio = 0.05f;
	auto pieceRotation = piece->getRotation();
	auto rotation = getRotation();
	float rotationRatio;

	switch (pieceShape)
	{
	case EShape::Square:
		pieceRotation = static_cast<int>(pieceRotation) % 90;
		rotation = static_cast<int>(rotation) % 90;
		rotationRatio = std::abs(rotation - pieceRotation) / 90;
		break;

	case EShape::Triangle:
		rotationRatio = std::abs(rotation - pieceRotation) / 360.f;
		break;

	case EShape::Circle:
		// Every rotation matches circle
		rotationRatio = 0.f;
		break;

	case EShape::Hexagon:
		pieceRotation = static_cast<int>(pieceRotation) % 90;
		rotation = static_cast<int>(rotation) % 90;
		rotationRatio = std::abs(rotation - pieceRotation) / 90.f;
		break;
	}

	if (rotationRatio > AllowedRotationRatio && rotationRatio < 1 - AllowedRotationRatio)
	{
		return false;
	}

	return true;
}

void PuzzleHole::setShape(EShape shape)
{
	m_shape = shape;
}

EShape PuzzleHole::getShape() const
{
	return m_shape;
}
