#ifndef PUZZLEHOLE_H
#define PUZZLEHOLE_H

#include "KimSprite.h"
#include "GlobalTypes.h"

class PuzzlePiece;
class PuzzleHole : public KimSprite
{
public:
	PuzzleHole();

	static PuzzleHole* create();
	static PuzzleHole* create(const char* fileName);

	bool canFit(PuzzlePiece* piece) const;

	void setShape(EShape shape);
	EShape getShape() const;

private:
	EShape m_shape;
};

#endif // PUZZLEHOLE_H